## Aplicación de registro de materias y alumnos para mesa de examen.
### Hecho por Nahuel Salgado y Cristhian Cantero.

### Para ingresar a la API de nuestras aplicaciones ir a:

*  http://localhost:8000/materia/
*  http://localhost:8000/registro_mesa_examen/

![Secretaría 2019](static/img/principal.png)

En nuestro ultimo año en la EPET Nº20 nos pidieron solucionar un problema de una determinada área. Así que aquí está lo que pudimos resolver.

Este trabajo es individual y con dos alumnos trabajando en un tema. Nosotros lo hicimos en un solo repositorio, pero optamos por hacerlo acá.

![Secretaría 2019](static/img/login.png)

Este proyecto hecho con ayuda de Django cuenta con un login que proteje los datos de usuarios no registrados en el sistema.

![Secretaría 2019](static/img/materias.png)

También cuenta con la capacidad de ver, crear, editar y borrar materias para luego poder seleccionarlas a la hora de crear una inscripción de un alumno para una mesa de examen.

![Secretaría 2019](static/img/alumnos_registrados.png)

De igual manera, en el sector de alumnos les ofrecemos un formulario para que los alumnos puedan registrarse a las mesas de examenes próximas. Como antes lo mencionamos, te da la capacidad de ver, crear, editar y borrar registros de los alumnos.