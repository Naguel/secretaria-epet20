# -*- coding: utf-8 -*-
from django.urls import include, path
from rest_framework import routers

from .views import (
	RegistroMesaExamenViewSet,
	AlumnoMesaExamenView,
	AlumnoMesaExamenCreate,
	AlumnoMesaExamenDetail,
	AlumnoMesaExamenDelete,
	AlumnoMesaExamenUpdate,
)

app_name = 'alumnos_mesas'

router = routers.DefaultRouter()
router.register('api', RegistroMesaExamenViewSet)

urlpatterns = [
	path('', include(router.urls)),
	path('listado_alumnos/', AlumnoMesaExamenView.as_view(), name='listado_alumnos'),
	path('formulario_registro/', AlumnoMesaExamenCreate.as_view(), name='formulario_alumno'),
	path('detalle_alumno/<int:pk>/', AlumnoMesaExamenDetail.as_view(), name='detalle_alumno'),
	path('borrar_alumno/<int:pk>/', AlumnoMesaExamenDelete.as_view(), name='borrar_alumno'),
	path('modificar_alumno/<int:pk>/', AlumnoMesaExamenUpdate.as_view(), name='modificar_alumno'),
]