# -*- coding: utf-8 -*-

from rest_framework import serializers
from .models import RegistroMesaExamen

class RegistroMesaExamenSerializer(serializers.ModelSerializer):

	class Meta:
		model = RegistroMesaExamen
		fields = ['nombre', 'apellido', 'dni', 'materia', 'correo_electronico']

