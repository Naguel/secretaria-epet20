# -*- coding: utf-8 -*-

from django.contrib import admin
from .models import RegistroMesaExamen

@admin.register(RegistroMesaExamen)
class RegistroMesaExamenAdmin(admin.ModelAdmin):
	list_display = ['nombre', 'apellido', 'dni', 'get_nombre_materia','get_año_materia', 'correo_electronico']
	list_filter = ['materia__nombre', 'materia__año']
	search_fields = ['dni']

	def get_nombre_materia(self, obj):
		return obj.materia.nombre
	get_nombre_materia.short_description = 'Nombre de la materia'

	def get_año_materia(self, obj):
		return obj.materia.año
	get_año_materia.short_description = 'Año de la materia'