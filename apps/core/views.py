from django.shortcuts import render
from django.views.generic import TemplateView


class GeneralView(TemplateView):
	template_name = 'general.html'
