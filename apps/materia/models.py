# -*- coding: utf-8 -*-

from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator

class Materia(models.Model):

	nombre = models.CharField(
		max_length=50,
		help_text='Ingrese el nombre de la materia.'
	)

	año = models.PositiveSmallIntegerField(
		validators=[
			MaxValueValidator(6),
			MinValueValidator(1)
		],
		help_text='Ingrese el año de la materia.'
	)

	fecha_hora = models.DateTimeField(
		'Fecha y Hora',
		help_text='Ingrese la fecha y hora a la que se rinde la materia. Formato: "DD/MM/AAAA HH:MM"'
	)

	aula = models.CharField(
		max_length=50,
		help_text='Ingrese el aula en la que se rinde la materia.'
	)

	profesores = models.CharField(
		max_length=150,
		help_text='Ingrese los profesores que evaluarán. Formato: "Nombre Apellido, Nombre Apellido, etc."'
	)

	class Meta:
		verbose_name='Materia'
		verbose_name_plural='Materias'

	def __str__(self):
		return '%s %sº' % (self.nombre, self.año)
