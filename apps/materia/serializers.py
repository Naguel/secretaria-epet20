# -*- coding: utf-8 -*-

from rest_framework import serializers
from .models import Materia

class MateriaSerializer(serializers.ModelSerializer):
	
	class Meta:

		model = Materia

		fields = (
			'id', 'nombre',
			'año', 'fecha_hora',
			'aula', 'profesores'
		)